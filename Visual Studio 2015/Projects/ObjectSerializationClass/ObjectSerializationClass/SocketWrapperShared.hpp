#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include "Windows.h"
#include "Winsock2.h"
#include "Ws2tcpip.h"
typedef int socklen_t;

#else
//includes for linux
#endif

#include "memory"
#include "vector"
#include "unordered_map"
#include "string"
#include "list"
#include "queue"
#include "deque"
#include "unordered_set"
#include "cassert"
#include "iostream"
#include "sstream"
#include "time.h"
#include "stdio.h"
#include "chrono"
#include "iomanip"


using std::shared_ptr;
using std::unique_ptr;
using std::vector;
using std::queue;
using std::list;
using std::deque;
using std::unordered_map;
using std::string;
using std::unordered_set;

enum PacketType
{
	PT_Hello,
	PT_ReplicationData,
	PT_Disconnect,
	PT_MAX
};

#include "StringUtils.hpp"
#include "SocketAddress.hpp"
#include "SocketAddressFactory.hpp"
#include "UDPSocket.hpp"
//#include "TCPSocket.hpp"
#include "SocketUtil.hpp"
#include "Math.hpp"
#include "ByteSwap.hpp"
#include "MemoryStream.hpp"
#include "MemoryBitStream.hpp"
#include "Player.hpp"
#include "GameObject.hpp"
#include "ComplexPlayer.hpp"
#include "LinkingContext.hpp"
