#include "SocketWrapperPCH.hpp"

string SocketAddress::ToString() const
{
#if _WIN32
	const sockaddr_in* s = GetAsSockAddrIn();
	char destinationbuffer[128];
	InetNtop(s->sin_family, const_cast<in_addr*>(&s->sin_addr), destinationbuffer, sizeof(destinationbuffer));
	return StringUtils::Sprintf("%s:%d", destinationbuffer, ntohs(s->sin_port));
#else
	return string("Haven't written the linux version yet");
#endif
}