class ComplexPlayer : public GameObject
{
public: 
	ComplexPlayer() : mHealth(10), mAmmo(3), mName("JohnDoe"), mPosition(10, 10, 10), mRotation(0, 0, 0, 1), mWeapons{ 60, 70, 80, 90, 100 } {}
	int getHealth() const;
	int getAmmo() const;
	void Write(OutputMemoryStream& outStream) const;
	void Read(InputMemoryStream& inStream);
	void WriteBits(OutputMemoryBitStream& outBitStream) const;
	void ReadBits(InputMemoryBitStream& instream);
	virtual void toString() const;

private:
	int32_t mHealth;
	int32_t mAmmo;
	char mName[128];
	Vector3 mPosition;
	Quaternion mRotation;
	vector<int> mWeapons;
};
