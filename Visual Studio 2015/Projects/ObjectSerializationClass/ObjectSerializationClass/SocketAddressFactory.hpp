class SocketAddressFactory
{
public:
	static SocketAddressPtr CreateIPv4FromString(const string& inString);
};
//Why just write this as a function in the SocketAddress class?
//What is the benefit of factories?
