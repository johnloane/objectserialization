#include "SocketWrapperPCH.hpp"

//Client can contact a server that offers three services
//Echo - just repeats what you send
//DateAndTime - sends back the date and time
//Stats - sends back how many queries the server has handled
void DoServiceLoop(UDPSocketPtr serverSocket);
void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, UDPSocketPtr serverSocket, int requests, bool &serviceRunning);
std::string ReturnCurrentDateAndTime();
int ConvertIPToInt(std::string ipString);
void NaivelyReceivePlayer(UDPSocketPtr serverSocket);
void ReceiveComplexPlayer(UDPSocketPtr serverSock);
void ReceivePlayerBits(UDPSocketPtr serverSock);
void ReceiveWorld(UDPSocketPtr serverSock, ComplexPlayer* player, LinkingContext gameContext);

enum Choice{ECHO = 1, DATEANDTIME =2, STATS = 3, NAIVELYSENDPLAYER = 4, SENDCOMPLEXPLAYER = 5, SENDCOMPRESSEDPLAYER = 6, SENDWORLD = 7, QUIT = 8};
const int32_t kMaxPacketSize = 1300;

int main()
{
	SocketUtil::StaticInit();
	UDPSocketPtr serverSocket = SocketUtil::CreateUDPSocket(INET);
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	serverSocket->Bind(serverAddress);
	serverSocket->SetNonBlockingMode(false);
	DoServiceLoop(serverSocket);
}

void DoServiceLoop(UDPSocketPtr serverSocket)
{
	bool serviceRunning = true;
	char receiveBuffer[kMaxPacketSize];
	//the next 3 lines set the buffer to null
	char *begin = receiveBuffer;
	char *end = begin + sizeof(receiveBuffer);
	std::fill(begin, end, 0);
	SocketAddress senderAddress;
	int bytesReceived = 0;
	int requests = 0;

	while (serviceRunning)
	{
		bytesReceived = serverSocket->ReceiveFrom(receiveBuffer, sizeof(receiveBuffer), senderAddress);
		if (bytesReceived > 0)
		{
			requests++;
			ProcessReceivedData(receiveBuffer, bytesReceived, senderAddress, serverSocket, requests, serviceRunning);
		}
	}
}

int ConvertIPToInt(std::string ipString)
{
	int intIP = 0;
	for (int i = 0; i < ipString.length(); i++)
	{
		if (ipString[i] == '.')
		{
			ipString[i] = ' ';
		}
	}
	vector<int> arrayTokens;
	std::stringstream ss(ipString);
	int temp;
	while (ss >> temp)
	{
		arrayTokens.emplace_back(temp);
	}

	for (int i = 0; i < arrayTokens.size(); i++)
	{
		intIP += (arrayTokens[i] << ((3 - i) * 8));
	}
	return intIP;
}

void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, UDPSocketPtr serverSocket, int requests, bool &serviceRunning)
{
	std::cout << "Got " << bytesReceived << " from " << socketAddress.ToString() << std::endl;
	std::cout << "The message is: " << receiveBuffer << std::endl;
	char responseData[kMaxPacketSize] = "";
	int choice = atoi(receiveBuffer);
	string currentDateAndTime = "";
	std::string requestsString = "";
	std::string quitString = "QUIT";
	ComplexPlayer* complexJohn = new ComplexPlayer();
	LinkingContext gameContext;

	switch (choice)
	{
	case ECHO:
		std::cout << "ECHO request" << std::endl;
		strcpy_s(responseData, receiveBuffer);
		break;
	case DATEANDTIME:
		std::cout << "DATEANDTIME request" << std::endl;
		currentDateAndTime = ReturnCurrentDateAndTime();
		currentDateAndTime.copy(responseData, currentDateAndTime.length(), 0);
		break;
	case STATS:
		std::cout << "STATS request" << std::endl;
		requestsString = std::to_string(requests);
		requestsString.copy(responseData, requestsString.length(), 0);
		break;
	case NAIVELYSENDPLAYER:
		std::cout << "NAIVELYSENDPLAYER request" << std::endl;
		NaivelyReceivePlayer(serverSocket);
		break;
	case SENDCOMPLEXPLAYER:
		std::cout << "SENDCOMPLEXPLAYER request" << std::endl;
		ReceiveComplexPlayer(serverSocket);
		break;
	case SENDCOMPRESSEDPLAYER:
		std::cout << "SENDCOMPRESSEDPLAYER request" << std::endl;
		ReceivePlayerBits(serverSocket);
		break;
	case SENDWORLD:
		std::cout << "SENDWORLD request" << std::endl;
		ReceiveWorld(serverSocket, complexJohn, gameContext);
		break;
	case QUIT:
		std::cout << "QUIT request" << std::endl;
		quitString.copy(responseData, quitString.length(), 0);
		serviceRunning = false;
		break;
	}
	int bytesSent = serverSocket->SendTo(responseData, sizeof(responseData), socketAddress);
}

std::string ReturnCurrentDateAndTime()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
	return ss.str();
}

void NaivelyReceivePlayer(UDPSocketPtr serverSocket)
{
	Player *receiver = new Player();
	SocketAddress senderAddress;
	int bytesReceived = 0;
	bytesReceived = serverSocket->ReceiveFrom(reinterpret_cast<char*>(receiver), sizeof(Player), senderAddress);
	receiver->toString();
	delete receiver;
}

void ReceiveComplexPlayer(UDPSocketPtr serverSock)
{
	ComplexPlayer *receiver = new ComplexPlayer();
	SocketAddress senderAddress;
	char* temporaryBuffer = static_cast<char*>(std::malloc(kMaxPacketSize));
	int receivedByteCount = serverSock->ReceiveFrom(temporaryBuffer, kMaxPacketSize, senderAddress);
	InputMemoryStream stream(temporaryBuffer, static_cast<uint32_t>(receivedByteCount));
	receiver->Read(stream);
	std::cout << "Received: " << receivedByteCount << std::endl;
	receiver->toString();
}

void ReceivePlayerBits(UDPSocketPtr serverSock)
{
	SocketAddress senderAddress;
	ComplexPlayer *receiver = new ComplexPlayer();
	char* temporaryBuffer = static_cast<char*>(std::malloc(kMaxPacketSize));
	int receivedByteCount = serverSock->ReceiveFrom(temporaryBuffer, kMaxPacketSize, senderAddress);
	InputMemoryBitStream stream(temporaryBuffer, static_cast<uint32_t>(receivedByteCount * 8));
	receiver->ReadBits(stream);
	std::cout << "Received: " << receivedByteCount << std::endl;
	receiver->toString();
}

void ReceiveWorld(UDPSocketPtr serverSock, ComplexPlayer* player, LinkingContext gameContext)
{
	SocketAddress senderAddress;
	char* temporaryBuffer = static_cast<char*>(std::malloc(kMaxPacketSize));
	int receivedByteCount = serverSock->ReceiveFrom(temporaryBuffer, kMaxPacketSize, senderAddress);
	InputMemoryBitStream stream(temporaryBuffer, static_cast<uint32_t>(receivedByteCount * 8));
	PacketType receivePacket;
	stream.ReadBits(&receivePacket, 2);
	uint32_t networkId;
	stream.ReadBits(&networkId, 32);
	if (gameContext.GetGameObject(networkId) == nullptr)
	{
		std::cout << "Object does not exists. Need to create it" << std::endl;
		int newId = gameContext.GetNetworkId(player, true);
		std::cout << "Object now has an id: " << newId << std::endl;
	}
	else
	{
		std::cout << "Object does exist. Copy data into it" << std::endl;
	}
	std::cout << "Received: " << receivePacket << std::endl;
	player->ReadBits(stream);
	std::cout << "Received: " << receivedByteCount << std::endl;
	player->toString();
}

