class Player
{
public:
	Player() : mHealth(10), mAmmo(3) {}
	Player(int Health, int Ammo) : mHealth(Health), mAmmo(Ammo) {}
	int getHealth() const;
	int getAmmo() const;
	int setHealth(int health);
	int setAmmo(int Ammo);
	virtual void toString() const;

private:
	uint32_t mHealth;
	uint32_t mAmmo;
};
