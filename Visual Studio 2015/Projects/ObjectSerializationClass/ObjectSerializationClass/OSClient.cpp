#include "SocketWrapperPCH.hpp"

//Client can contact a server that offers three services
//Echo - just repeats what you send
//DateAndTime - sends back the date and time
//Stats - sends back how many queries the server has handled
void DoServiceLoop(UDPSocketPtr clientSocket);
void PrintOptions();
void GetChoice(string &choice);
void SendDataToServer(UDPSocketPtr clientSocket, char* input);
void ReceiveDataFromServer(UDPSocketPtr clientSocket, char* receiveBuffer, SocketAddress socketAddress, int bytesReceived, bool &serviceRunning);
void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, bool &serviceRunning);
int ConvertIPToInt(std::string ipString);
void NaivelySendPlayer(UDPSocketPtr clientSocket, Player* player);
void SendComplexPlayerUsingStream(UDPSocketPtr clientSocket, const ComplexPlayer* player);
void SendPlayerBits(UDPSocketPtr clientSocket, const ComplexPlayer* player);
void SendWorld(UDPSocketPtr clientSocket, ComplexPlayer* player, LinkingContext gameContext);
const int32_t kMaxPacketSize = 1300;
const int8_t NAIVELYSENDPLAYER = 4;
const int8_t SENDCOMPLEXPLAYER = 5;
const int8_t SENDCOMPRESSEDPLAYER = 6;
const int8_t SENDWORLD = 7;

int main()
{
	SocketUtil::StaticInit();
	UDPSocketPtr clientSocket = SocketUtil::CreateUDPSocket(INET);
	clientSocket->SetNonBlockingMode(false);
	DoServiceLoop(clientSocket);
}

void DoServiceLoop(UDPSocketPtr clientSocket)
{
	bool serviceRunning = true;
	string choice;
	char receiveBuffer[kMaxPacketSize];
	//the next 3 lines set the buffer to null
	char *begin = receiveBuffer;
	char *end = begin + sizeof(receiveBuffer);
	std::fill(begin, end, 0);
	SocketAddress senderAddress;
	int bytesReceived = 0;
	Player* john = new Player(100, 3000);
	ComplexPlayer* complexJohn = new ComplexPlayer();
	LinkingContext gameContext;

	while (serviceRunning)
	{
		PrintOptions();
		GetChoice(choice);

		SendDataToServer(clientSocket, (char *)choice.c_str());
		if (std::stoi(choice) == NAIVELYSENDPLAYER)
		{
			NaivelySendPlayer(clientSocket, john);
		}
		else if (std::stoi(choice) == SENDCOMPLEXPLAYER)
		{
			SendComplexPlayerUsingStream(clientSocket, complexJohn); 
		}
		else if (std::stoi(choice) == SENDCOMPRESSEDPLAYER)
		{
			SendPlayerBits(clientSocket, complexJohn);	
		}
		else if (std::stoi(choice) == SENDWORLD)
		{
			SendWorld(clientSocket, complexJohn, gameContext);
		}
		ReceiveDataFromServer(clientSocket, receiveBuffer, senderAddress, bytesReceived, serviceRunning);
	}
}

void PrintOptions()
{
	std::cout << "Please enter: " << std::endl;
	std::cout << "1) To use the EHCO service: " << std::endl;
	std::cout << "2) To use the DATEANDTIME service: " << std::endl;
	std::cout << "3) To use the STATS service: " << std::endl;
	std::cout << "4) To naively send player: " << std::endl;
	std::cout << "5) To send complex player" << std::endl;
	std::cout << "6) To send complex player using compression" << std::endl;
	std::cout << "7) To send world using object replication" << std::endl;
	std::cout << "8) To quit: " << std::endl;
}

void GetChoice(string &choice)
{
	std::getline(std::cin, choice);
}

void SendDataToServer(UDPSocketPtr clientSocket, char* input)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	int bytesSent = clientSocket->SendTo(input, sizeof(input), serverAddress);
}

int ConvertIPToInt(std::string ipString)
{
	int intIP = 0;
	for (int i = 0; i < ipString.length(); i++)
	{
		if (ipString[i] == '.')
		{
			ipString[i] = ' ';
		}
	}
	vector<int> arrayTokens;
	std::stringstream ss(ipString);
	int temp;
	while (ss >> temp)
	{
		arrayTokens.emplace_back(temp);
	}

	for (int i = 0; i < arrayTokens.size(); i++)
	{
		intIP += (arrayTokens[i] << ((3 - i) * 8));
	}
	return intIP;
}

void ReceiveDataFromServer(UDPSocketPtr clientSocket, char* receiveBuffer, SocketAddress socketAddress, int bytesReceived, bool &serviceRunning)
{
	std::cout << sizeof(receiveBuffer) << std::endl;
	bytesReceived = clientSocket->ReceiveFrom(receiveBuffer, kMaxPacketSize, socketAddress);
	if (bytesReceived > 0)
	{
		ProcessReceivedData(receiveBuffer, bytesReceived, socketAddress, serviceRunning);
	}
}

void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, bool &serviceRunning)
{
	char key[] = "QUIT";

	if (strcmp(key, receiveBuffer) == 0)
	{
		std::cout << "Server says we need to shut down...." << std::endl;
		serviceRunning = false;
	}

	std::cout << "Got " << bytesReceived << " from " << socketAddress.ToString() << std::endl;
	std::cout << "The message is: " << receiveBuffer << std::endl;
}

void NaivelySendPlayer(UDPSocketPtr clientSocket, Player* player)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	int bytesSent = clientSocket->SendTo(reinterpret_cast<const char*>(player), sizeof(Player), serverAddress);
}

void SendComplexPlayerUsingStream(UDPSocketPtr clientSocket, const ComplexPlayer* player)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryStream outStream;
	player->Write(outStream);
	int bytesSent = clientSocket->SendTo(outStream.GetBufferPtr(), outStream.GetLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

void SendPlayerBits(UDPSocketPtr clientSocket, const ComplexPlayer* player)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryBitStream outBitStream;
	player->WriteBits(outBitStream);
	int bytesSent = clientSocket->SendTo(outBitStream.GetBufferPtr(), outBitStream.GetByteLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

void SendWorld(UDPSocketPtr clientSocket, ComplexPlayer* player, LinkingContext gameContext)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryBitStream outBitStream;
	//First send the type of the packet
	PacketType orPacket = PT_ReplicationData;
	outBitStream.WriteBits(&orPacket, 2);
	//Second send the id of the packet
	uint32_t networkId = gameContext.GetNetworkId(player, true);
	std::cout << "Id is: " << networkId << std::endl;
	outBitStream.WriteBits(networkId, 32);
	player->WriteBits(outBitStream);
	int bytesSent = clientSocket->SendTo(outBitStream.GetBufferPtr(), outBitStream.GetByteLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

